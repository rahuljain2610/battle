const model = require('../model');
const successResponse = require('../misc/successResponse');
exports.list = async (req, res) => {
    try {
        var placeList = await model.battle.placeList();
    } catch (e) {
        res.status(500).send({ error: 'Something went wrong!Please try again' })
    }
    res.send(successResponse(placeList))
}
exports.battleCounts = async (req, res) => {
    try {
        var battleCounts = await model.battle.battleCounts();

    } catch (e) {
        res.status(500).send({ error: 'Something went wrong!Please try again' })
    }
    res.send(successResponse(battleCounts))
}
exports.battleStats = async (req, res) => {
    try {
        mostActivePlace = model.battle.battleMostActiveStats('region');
        mostActiveAttack = model.battle.battleMostActiveStats('attacker_king');
        mostActiveDefence = model.battle.battleMostActiveStats('defender_king');
        mostActiveName = model.battle.battleMostActiveStats('name');
        outcomes = model.battle.attackerOutcomes();
        battleTypes = model.battle.distinctBattleTypes();
        defenderSize = model.battle.defenderSize();
        var battleStats = await Promise.all([outcomes, battleTypes, defenderSize, mostActivePlace, mostActiveAttack, mostActiveDefence, mostActiveName]);
    } catch (e) {
        res.status(500).send({ error: 'Something went wrong!Please try again' })
    }
    let battleObject = {
        attacker_outcomes: battleStats[0],
        battleTypes: battleStats[1],
        defenderSizeStats: battleStats[2],
        mostActiveStats: {
            'attacker_king':battleStats[4],
            'defender_king':battleStats[5],
            'region':battleStats[3],
            'name':battleStats[6]
            }
    }
    res.send(successResponse(battleObject))
}

exports.search = async (req, res) => {
    try {
        let queryObject = createSearchQuery(req.query);
        var results = await model.battle.search(queryObject);
    } catch (e) {
        res.status(500).send({ error: 'Something went wrong!Please try again' })
    }
    res.send(successResponse(results))
}
const createSearchQuery = (query)=>{
    let queryObject = {};
    let multipleQueries = [];
    if(query && Object.keys(query).length){
        for(let queryField of Object.keys(query)){
            multipleQueries.push(createkeyQueries(query,queryField));
        }
    }
        queryObject['$and'] = multipleQueries;
        return queryObject;
}
const createkeyQueries = (query,key,)=>{
    let orQueries = [];
    for(let field of searchFields[key]){
        orQueries.push({[field]:query[key]});
    }
    return {'$or':orQueries}
}
const searchFields = {'king':['attacker_king','defender_king'],'type':['battle_type'],'location':['location','region']};//All fields could be added here to allow in search query.