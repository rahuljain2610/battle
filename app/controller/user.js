const model = require('../model');
const jwt = require('jsonwebtoken');
const successResponse = require('../misc/successResponse');
exports.register = async (req, res) => {
    let body = req.body;
    let userInstance = new model.user(body);
    try {
        var exists = await model.user.checkIfUsernameExists(body.username);
    } catch (e) {
        res.status(500).send({ error: 'Something went wrong!Please try again' })
    }
    if (exists) {
        res.status(401).send({ error: 'Username Already Exists' })
    } else {
        userInstance.save((err, user) => {
            if (err) {
                res.status(500).send({ error: 'Something went wrong!Please try again' })
            }
            res.send(successResponse(exists));
        })
    }
}
exports.login = async(req, res) => {
    let body = req.body;
   let userDetails;
    try {
        userDetails = await model.user.getUserInfoByUsernamePassword(body.username, body.password);
    } catch (e) {
        res.status(500).send({ error: 'Something went wrong!Please try again' })
    }
    if (userDetails) {
        userDetails = userDetails["_doc"]
       res.send(successResponse(createUserToken(userDetails)))
    } else {
        res.status(401).send({ error: 'Invalid Username/Password' })
        
    }
}
const createUserToken = (userInfo) => {
    let token = jwt.sign(userInfo, "secret");
    return token;
}

