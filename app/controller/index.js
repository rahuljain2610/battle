const fs = require("fs");
const path = require("path");
let controller = {};

//Read all controller files and create a controller Object.

fs
    .readdirSync(__dirname)
    .filter(file => {
        return (file.indexOf(".") !== 0) && (file !== "index.js");
    })
    .forEach(file => {
        let file_name = file.replace(".js", "");
        controller[file_name] = require(path.join(__dirname, file));
    });

module.exports = controller;