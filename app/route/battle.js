const express = require('express');
const router = express.Router();
const controller = require(`../controller`);
const authenticateUser = require('../middleware/authenticate');
router.get('/list',authenticateUser.authenticate,controller.battle.list);
router.get('/count',authenticateUser.authenticate,controller.battle.battleCounts);
router.get('/stats',authenticateUser.authenticate,controller.battle.battleStats);
router.get('/search',authenticateUser.authenticate,controller.battle.search);
module.exports = router;