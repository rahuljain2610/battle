const express = require('express');
const router = express.Router();
const controller = require(`../controller`);
const authenticateUser = require('../middleware/authenticate');
router.post('/register',controller.user.register);
router.post('/login',controller.user.login);
module.exports = router;