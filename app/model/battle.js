const mongoose = require('mongoose')
const schema = require('../schema');
const model = require('../model');
const battleModel = mongoose.model('gameofthrones', schema.battle);


battleModel.placeList = async () => {
        return await battleModel.distinct("location")
}

battleModel.battleCounts = async () => {
        return await battleModel.count();
}

battleModel.battleMostActiveStats = async (field) => {
    return await  battleModel.aggregate()
    .group({ _id: `$${field}`, count: { $sum: 1 }})
    .sort({ count: -1 })
    .limit(1)
    // .project({'$_id':'$count'})
}

battleModel.attackerOutcomes = async () => {
    return await battleModel.aggregate()
    .group({ _id: "$attacker_outcome", count: { $sum: 1 }})
    // .project({})
}

battleModel.distinctBattleTypes = async () => {
    return await  battleModel.distinct("battle_type")
}


battleModel.defenderSize = async () => {
    return await battleModel.aggregate()
    .group({_id:null,max: { $max: "$defender_size" }, min: { $min: "$defender_size" }, avg: { $avg: "$defender_size" }})
    
}



battleModel.search = async (queryObject) => {
      return await battleModel.find(queryObject)
}




module.exports = battleModel;