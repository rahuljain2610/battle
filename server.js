const express = require('express')
const app = express();
const config = require(`./app/config`);
const middleware = require('./app/middleware')
const routes = require('./app/route');
const bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.get('/', (req, res, next) => {
    res.send('Welcome Aboard');
    next();
})

app.use('/', routes);
app.listen(process.env.PORT || config.get('port'), () => {
    console.log(`Server Initiated on ${config.get('port')}`)
})