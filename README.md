
## API Collection And Details

- Postman Collection Link : https://www.getpostman.com/collections/b65f7b53d5410b1cd3f1

- Postman Production Environment: battleProduction.postman_environment.json

## Other Notes

- These APIs are deployed on heroku

- heroku URL - https://afternoon-ridge-41522.herokuapp.com

- Extra /login API is created for JWT integration User registeration and login

- Mongodb is hosted on mlabs. (https://mlab.com/)